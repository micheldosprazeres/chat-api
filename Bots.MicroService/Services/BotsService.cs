﻿namespace Bots.Services
{
    using Bots.Domain.Entities;
    using Bots.Domain.Entities.Result;
    using Bots.Domain.Repositories;
    using Bots.Domain.Services;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    //[MicroService]
    public class BotsService: IBotsService
    {
        #region Variables
        private readonly IBotsRepository _botsRepository;
        #endregion

        public BotsService(IBotsRepository botsRepository)
        {
            _botsRepository = botsRepository;
        }

        public async Task<Result> DeleteAsync(Guid id)
        {

            try
            {

                if (Guid.Empty.Equals(id))
                {
                    return new Result
                    {
                        Message = "O id do bot deve ser informado.",
                        Success = false
                    };
                }

                var responseGet = await GetAsync(id);
                if (null == responseGet)
                {
                    return new Result
                    {
                        Message = "Não foi possível obter o bot informado.",
                        Success = false
                    };
                }

                await _botsRepository.DeleteAsync(id);

                return new Result
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new Result
                {
                    Message = "Falha ao deletar o bot.",
                    Success = false
                };
            }
            
        }

        public async Task<BotResult<IEnumerable<Bot>>> GetAllAsync()
        {
            try
            {
                var bot = await _botsRepository.GetAllAsync();

                return new BotResult<IEnumerable<Bot>>
                {
                    Success = true,
                    Data = bot
                };
            }
            catch (Exception ex)
            {
                return new BotResult<IEnumerable<Bot>>
                {
                    Message = "Falha ao obter a lista de bots.",
                    Success = false
                };
            }
        }

        public async Task<BotResult<Bot>> GetAsync(Guid id)
        {
            try
            {
                var bot = await _botsRepository.GetAsync(id);

                return new BotResult<Bot>
                {
                    Success = true,
                    Data = bot
                };
            }
            catch (Exception ex)
            {
                return new BotResult<Bot>
                {
                    Message = "Falha ao obter o bot.",
                    Success = false
                };
            }
        }

        public async Task<Result> NewAsync(Bot bot)
        {
            try
            {
                if (string.IsNullOrEmpty(bot.Message))
                {
                    return new Result
                    {
                        Message = "A mensagem deve ser informada.",
                        Success = false
                    };
                }

                await _botsRepository.NewAsync(bot);

                return new Result
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new Result
                {
                    Message = "Falha ao criar o bot.",
                    Success = false
                };
            }

        }

        public async Task<Result> UpdateAsync(Bot bot)
        {
            try
            {
                if (Guid.Empty.Equals(bot.Id))
                {
                    return new Result
                    {
                        Message = "O id do bot deve ser informado.",
                        Success = false
                    };
                }

                if (string.IsNullOrEmpty(bot.Message))
                {
                    return new Result
                    {
                        Message = "A mensagem deve ser informada.",
                        Success = false
                    };
                }

                var responseGet = await GetAsync(bot.Id);
                if (null == responseGet)
                {
                    return new Result
                    {
                        Message = "Não foi possível obter o bot informado.",
                        Success = false
                    };
                }

                await _botsRepository.UpdateAsync(bot);

                return new Result
                {
                    Success = true
                };

            }
            catch (Exception)
            {
                return new Result
                {
                    Message = "Falha ao atualizar o bot.",
                    Success = false
                };
            }
        }
    }
}
