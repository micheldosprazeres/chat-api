﻿namespace Bots.Services
{
    using Bots.Domain.Entities;
    using Bots.Domain.Entities.Result;
    using Bots.Domain.Repositories;
    using Bots.Domain.Services;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    //[MicroService]
    public class MessagesService: IMessagesService
    {
        #region Variables
        private readonly IMessagesRepository _messagesRepository;
        #endregion

        public MessagesService(IMessagesRepository messagesRepository)
        {
            _messagesRepository = messagesRepository;
        }

        public async Task<Result> DeleteAsync(Message message)
        {

            try
            {

                if (Guid.Empty.Equals(message.Id))
                {
                    return new Result
                    {
                        Message = "O id do message deve ser informado.",
                        Success = false
                    };
                }

                var responseGet = await GetAsync(message.Id);
                if (null == responseGet)
                {
                    return new Result
                    {
                        Message = "Não foi possível obter o message informado.",
                        Success = false
                    };
                }

                await _messagesRepository.DeleteAsync(message.Id);

                return new Result
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new Result
                {
                    Message = "Falha ao remover a mensagem.",
                    Success = false
                };
            }
            
        }

        public async Task<BotResult<IEnumerable<Message>>> GetAllAsync()
        {
            try
            {
                var message = await _messagesRepository.GetAllAsync();

                return new BotResult<IEnumerable<Message>>
                {
                    Success = true,
                    Data = message
                };
            }
            catch (Exception ex)
            {
                return new BotResult<IEnumerable<Message>>
                {
                    Message = "Falha ao obter a lista de mensagens.",
                    Success = false
                };
            }
        }

        public async Task<BotResult<Message>> GetAsync(Guid id)
        {
            try
            {
                var message = await _messagesRepository.GetAsync(id);

                return new BotResult<Message>
                {
                    Success = true,
                    Data = message
                };
            }
            catch (Exception ex)
            {
                return new BotResult<Message>
                {
                    Message = "Falha ao obter a mensagem.",
                    Success = false
                };
            }
        }

        public async Task<Result> NewAsync(Message message)
        {
            try
            {
                var validate = Validate(message);

                if (!validate.Success)
                {
                    return new Result
                    {
                        Message = validate.Message,
                        Success = false
                    };
                }

                await _messagesRepository.NewAsync(message);

                return new Result
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new Result
                {
                    Message = "Falha ao criar a mensagem.",
                    Success = false
                };
            }

        }

        public async Task<Result> UpdateAsync(Message message)
        {
            try
            {

                var validate = Validate(message);

                if (!validate.Success)
                {
                    return new Result
                    {
                        Message = validate.Message,
                        Success = false
                    };
                }
                
                var responseGet = await GetAsync(message.Id);
                if (null == responseGet)
                {
                    return new Result
                    {
                        Message = "Não foi possível obter a mensagem.",
                        Success = false
                    };
                }

                await _messagesRepository.UpdateAsync(message);

                return new Result
                {
                    Success = true
                };

            }
            catch (Exception)
            {
                return new Result
                {
                    Message = "Falha ao atualizar a mensagem.",
                    Success = false
                };
            }
        }


        private Result Validate(Message message)
        {
            if (string.IsNullOrEmpty(message.Text))
            {
                return new Result
                {
                    Message = "A mensagem deve ser informada.",
                    Success = false
                };
            }

            if (string.IsNullOrEmpty(message.From))
            {
                return new Result
                {
                    Message = "O remetente deve ser informado.",
                    Success = false
                };
            }

            if (string.IsNullOrEmpty(message.To))
            {
                return new Result
                {
                    Message = "O destinatário deve ser informado.",
                    Success = false
                };
            }

            if (Guid.Empty.Equals(message.ConversationId))
            {
                return new Result
                {
                    Message = "O id da conversa deve ser informado.",
                    Success = false
                };
            }

            return new Result
            {
                Success = true
            };
        }
    }
}
