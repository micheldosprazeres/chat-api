﻿namespace Bots.Repository.Repositories
{
    using Bots.Domain.Entities;
    using Bots.Domain.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class MessagesRepository : IMessagesRepository
    {
        #region Constructors
        public MessagesRepository() 
        {
        }
        #endregion

        public async Task UpdateAsync(Message bot)
        {
            throw new Exception();
        }

        public async Task DeleteAsync(Guid id)
        {
            throw new Exception();
        }

        public async Task<Message>  GetAsync(Guid id)
        {
            throw new Exception();
        }

        public async Task<IEnumerable<Message>> GetAllAsync()
        {
            throw new Exception();
        }

        public async Task  NewAsync(Message bot)
        {
            throw new Exception();
        }
    }
}
