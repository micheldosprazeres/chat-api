﻿namespace Bots.Repository.Repositories
{
    using Bots.Domain.Entities;
    using Bots.Domain.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class BotsRepository : IBotsRepository
    {
        
        #region Constructors
        public BotsRepository() 
        {
        }
        #endregion

        public async Task UpdateAsync(Bot bot)
        {
            throw new Exception();
        }

        public async Task DeleteAsync(Guid id)
        {
            throw new Exception();
        }

        public async Task<Bot>  GetAsync(Guid id)
        {
            throw new Exception();
        }

        public async Task<IEnumerable<Bot>> GetAllAsync()
        {
            throw new Exception();
        }

        public async Task  NewAsync(Bot bot)
        {
            throw new Exception();
        }
    }
}
