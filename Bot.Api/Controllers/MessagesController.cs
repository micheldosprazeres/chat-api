﻿using AutoMapper;
using Bots.Api.App_Start;
using Bots.Api.Model;
using Bots.Domain.Services;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Bots.Api.Controllers
{

    /// <summary>
    /// Message Api
    /// </summary>
    [RoutePrefix("api/v1")]
    public class MessagesController : ApiController
    {

        #region Variables
        private readonly IMapper _mapper;
        private readonly IMessagesService _messageService;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public MessagesController(IMessagesService messageService)
        {
            _messageService = messageService;
            _mapper = MapperConfig.Instance;
        }

        /// <summary>
        /// Creates an message using the available parameters.
        /// </summary>
        /// <param name="messageRequest">message to be created.</param>
        /// <returns>A single message based in the chosen filter.</returns>
        /// <response code="201">Creates a message.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while creating the message.</response>
        [HttpPost]
        [Route("messages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("CreateMessage")]
        public async Task<IHttpActionResult> PostAsync(MessageResponse message)
        {

            try
            {

                var messageResponse = await _messageService.NewAsync(new Domain.Entities.Message
                {
                    Id = message.Id,
                    ConversationId = message.ConversationId,
                    From = message.From,
                    Text =message.Text,
                    To = message.To
                });

                if (!messageResponse.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = messageResponse.Message,
                    });
                }

                return Content(HttpStatusCode.Created, true);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }


        /// <summary>
        /// Gets a single message using the available filters.
        /// </summary>
        /// <param name="id">Defines the message to be returned.</param>
        /// <returns>A single message based in the chosen filter.</returns>
        /// <response code="200">Returns a single message based in the chosen filter.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while getting the message.</response>
        [HttpGet]
        [Route("messages/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(MessageResponse))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("GetMessage")]
        public async Task<IHttpActionResult> GetAsync(Guid id)
        {

            try
            {
  
                var response = await _messageService.GetAsync(id);

                if (!response.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = response.Message,
                    });
                }


                return Content(HttpStatusCode.OK,
                    _mapper.Map<MessageResponse>(response.Data)
                );

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }



        /// <summary>
        /// Gets a list of message using the available filters.
        /// </summary>
        /// <returns>A list of messages.</returns>
        /// <response code="200">Returns a list of messages.</response>
        /// <response code="500">If any error occurrs while getting the message.</response>
        [HttpGet]
        [Route("messages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<MessageResponse>))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("GetAllMessages")]
        public async Task<IHttpActionResult> GetAllAsync()
        {

            try
            {

                var response = await _messageService.GetAllAsync();

                if (!response.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = response.Message,
                    });
                }


                return Content(HttpStatusCode.OK,
                    _mapper.Map<IEnumerable<MessageResponse>>(response.Data)
                );

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }
    }
}
