﻿using AutoMapper;
using Bots.Api.App_Start;
using Bots.Api.Model;
using Bots.Domain.Entities;
using Bots.Domain.Services;
using Bots.Services;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Bots.Api.Controllers
{

    /// <summary>
    /// Bot Api
    /// </summary>
    [RoutePrefix("api/v1")]
    public class BotsController : ApiController
    {

        #region Variables
        private readonly IMapper _mapper;
        private readonly IBotsService _botService;
        #endregion
        

    public BotsController(IBotsService botService)
        {
            _botService = botService;
            _mapper = MapperConfig.Instance;
        }

        /// <summary>
        /// Creates an bot using the available parameters.
        /// </summary>
        /// <param name="botRequest">bot to be created.</param>
        /// <returns>A single bot based in the chosen filter.</returns>
        /// <response code="201">Creates a bot.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while creating the bot.</response>
        [HttpPost]
        [Route("bots")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("CreateBot")]
        public async Task<IHttpActionResult> PostAsync(BotResponse bot)
        {
            try
            {
                var response = await _botService.NewAsync(new Domain.Entities.Bot
                {
                    Id = bot.Id,
                    Message = bot.Message
                });

                if (!response.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = response.Message,
                    });
                }

                return Content(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }


        /// <summary>
        /// Gets a single bot using the available filters.
        /// </summary>
        /// <param name="id">Defines the bot to be returned.</param>
        /// <returns>A single bot based in the chosen filter.</returns>
        /// <response code="200">Returns a single bot based in the chosen filter.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while getting the bot.</response>
        [HttpGet]
        [Route("bots/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BotResponse))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("GetBot")]
        public async Task<IHttpActionResult> GetAsync(Guid id)
        {

            try
            {

                var response = await _botService.GetAsync(id);

                if (!response.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = response.Message,
                    });
                }


                return Content(HttpStatusCode.OK,
                    _mapper.Map<BotResponse>(response.Data)
                );

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }



        /// <summary>
        /// Gets a list of bot using the available filters.
        /// </summary>
        /// <returns>A list of bots.</returns>
        /// <response code="200">Returns a list of bots.</response>
        /// <response code="500">If any error occurrs while getting the bot.</response>
        [HttpGet]
        [Route("bots")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<BotResponse>))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("GetAllBots")]
        public async Task<IHttpActionResult> GetAllAsync()
        {

            try
            {

                var response = await _botService.GetAllAsync();

                if (!response.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = response.Message,
                    });
                }


                return Content(HttpStatusCode.OK,
                    _mapper.Map<IEnumerable<BotResponse>>(response.Data)
                );

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }

        /// <summary>
        /// Updates an bot using the available parameters.
        /// </summary>
        /// <param name="id">Bot id to be updated.</param>
        /// <param name="message">message to be updated.</param>
        /// <returns>returns true if the bot is updated.</returns>
        /// <response code="201">returns true if the bot is updated.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while updating the bot.</response>
        [HttpPut]
        [Route("bots/{id}")]
        [SwaggerResponse(HttpStatusCode.Created, Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("UpdateBot")]
        public async Task<IHttpActionResult> PutAsync(Guid id, string message)
        {
            try
            {

                var botResponse = await _botService.UpdateAsync(new Domain.Entities.Bot
                {
                    Id = id,
                    Message = message
                });

                if (!botResponse.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = botResponse.Message,
                    });
                }

                return Content(HttpStatusCode.Created, true);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }


        /// <summary>
        /// Deletes an bot using the available parameters.
        /// </summary>
        /// <param name="id">Bot id.</param>
        /// <returns>returns true if the bot is deleted</returns>
        /// <response code="200">returns true if the bot is deleted.</response>
        /// <response code="412">If the information provided are not consistent</response>
        /// <response code="500">If any error occurrs while deleting the bot.</response>
        [HttpDelete]
        [Route("bots/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.PreconditionFailed, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(ResponseApiError))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(ResponseApiError))]
        [SwaggerOperation("DeleteBot")]
        public async Task<IHttpActionResult> DeleteAsync(Guid id)
        {
            try
            {

                var responseDelete = await _botService.DeleteAsync(id);
                if (!responseDelete.Success)
                {
                    return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                    {
                        Message = responseDelete.Message,
                    });
                }

                return Content(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new ResponseApiError
                {
                    Message = "Ocorreu uma falha ao processar a sua solicitação.",
                    MoreInfo = ex.Message
                });
            }
        }
    }
}
