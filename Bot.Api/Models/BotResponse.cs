﻿namespace Bots.Api.Model
{
    using Newtonsoft.Json;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class BotResponse
    {
        #region Properties
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("message")]
        public string Message{ get; set; }
        #endregion

        public override string ToString()
        {
            return $@"Id: {Id} / 
                    Message: {Message}";
        }
    }
}
