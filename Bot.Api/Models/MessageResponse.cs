﻿namespace Bots.Api.Model
{
    using Newtonsoft.Json;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class MessageResponse
    {
        #region Properties
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("conversationId")]
        public Guid ConversationId { get; set; }
        
        [JsonProperty("timeStamp")]
        public string TimeStamp { get; set; }

        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
        #endregion

        public override string ToString()
        {
            return $@"Id: {Id} / 
                    ConversationId: {ConversationId} /
                    TimeStamp: {TimeStamp} /
                    From: {From} /
                    To: {To} /
                    Text: {Text}";

        }
    }
}
