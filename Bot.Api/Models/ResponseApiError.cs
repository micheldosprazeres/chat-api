﻿
namespace Bots.Api.Model
{
    using Newtonsoft.Json;

    public class ResponseApiError
    {

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("moreInfo")]
        public string MoreInfo { get; set; }

    }
}
