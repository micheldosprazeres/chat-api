﻿using Bots.Domain.Repositories;
using Bots.Domain.Services;
using Bots.Repository.Repositories;
using Bots.Services;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Bot.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);



            var container = new Container();
            container.Register<IBotsService, BotsService>(Lifestyle.Singleton);
            container.Register<IMessagesService, MessagesService>(Lifestyle.Singleton);
            container.Register<IBotsRepository, BotsRepository>(Lifestyle.Singleton);
            container.Register<IMessagesRepository, MessagesRepository>(Lifestyle.Singleton);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration); 
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
