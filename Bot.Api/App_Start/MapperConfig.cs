﻿using AutoMapper;
using Bots.Api.App_Start;
using Bots.Api.Model;
using Bots.Domain.Entities;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(MapperConfig), "Register")]
namespace Bots.Api.App_Start
{
    public class MapperConfig
    {
        #region Variables

        private static IMapper instance;

        #endregion

        #region Properties

        public static IMapper Instance
        {
            get
            {
                if (instance == null)
                {
                    Register();
                }

                return instance;
            }
        }

        #endregion

        #region Constructors

        protected MapperConfig()
        {

        }

        #endregion

        #region Methods

        public static void Register()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Bots.Domain.Entities.Bot, BotResponse>();
                cfg.CreateMap<Message, MessageResponse>();
            });

            instance = Mapper.Instance;
        }

        #endregion
    }
}
