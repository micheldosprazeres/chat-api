﻿namespace Bots.Domain.Repositories
{
    using Bots.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IBotsRepository
    {
        Task NewAsync(Bot bot);
        Task DeleteAsync(Guid id);
        Task UpdateAsync(Bot bot);
        Task<Bot> GetAsync(Guid id);
        Task<IEnumerable<Bot>> GetAllAsync();
    }
}