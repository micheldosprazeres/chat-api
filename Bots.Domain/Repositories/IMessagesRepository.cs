﻿namespace Bots.Domain.Repositories
{
    using Bots.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IMessagesRepository
    {
        Task NewAsync(Message message);
        Task DeleteAsync(Guid id);
        Task UpdateAsync(Message message);
        Task<Message> GetAsync(Guid id);
        Task<IEnumerable<Message>> GetAllAsync();
    }
}