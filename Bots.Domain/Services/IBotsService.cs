﻿using Bots.Domain.Entities;
using Bots.Domain.Entities.Result;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bots.Domain.Services
{

    public interface IBotsService
    {
        Task<Result> NewAsync(Bot bot);
        Task<Result> DeleteAsync(Guid id);
        Task<Result> UpdateAsync(Bot bot);
        Task<BotResult<Bot>> GetAsync(Guid id);
        Task<BotResult<IEnumerable<Bot>>> GetAllAsync();
    }
}