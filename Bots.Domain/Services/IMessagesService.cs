﻿using Bots.Domain.Entities;
using Bots.Domain.Entities.Result;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bots.Domain.Services
{

    public interface IMessagesService
    {
        Task<Result> NewAsync(Message bot);
        Task<Result> DeleteAsync(Message bot);
        Task<Result> UpdateAsync(Message bot);
        Task<BotResult<Message>> GetAsync(Guid id);
        Task<BotResult<IEnumerable<Message>>> GetAllAsync();
    }
}