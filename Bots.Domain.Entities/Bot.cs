﻿using System;

namespace Bots.Domain.Entities
{
    public class Bot
    {
        #region Properties
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        #endregion

        public override string ToString()
        {
            return $@"Id: {Id} / 
                    Message: {Message} /
                    CreatedAt: {CreatedAt} /
                    UpdatedAt: {UpdatedAt}";
        }
    }
}
