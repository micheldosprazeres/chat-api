﻿namespace Bots.Domain.Entities
{
    using System;

    public class Message
    {
        #region Properties
        public Guid Id { get; set; }

        public Guid ConversationId { get; set; }

        public string TimeStamp { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $@"Id: {Id} / 
                    ConversationId: {ConversationId} /
                    TimeStamp: {TimeStamp} /
                    From: {From} /
                    To: {To} /
                    CreatedAt: {CreatedAt} /
                    UpdatedAt: {UpdatedAt} /
                    Text: {Text}";
        }
        #endregion

    }
}
