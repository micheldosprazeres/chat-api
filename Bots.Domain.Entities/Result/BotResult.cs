﻿namespace Bots.Domain.Entities.Result
{
    public class BotResult<T>: Result
    {
        public T Data { get; set; }
    }
}
