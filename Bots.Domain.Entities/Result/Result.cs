﻿namespace Bots.Domain.Entities.Result
{
    public class Result
    {
        public string Message  { get; set; }
        public bool Success { get; set; }
    }
}
