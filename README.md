# chat-api
A arquitetura foi criada usando como base o conceito Domain Driven Design e o desenvolvimento 
foi feito para mostrar um caminho minimamente viável para atender a necessidade e apresentar o codestyle. 
A camada de repositório não acessa os dados de fato.

*Bots.Domain*
Nessa camada está a definição das interfaces e assuntos relacionados ao negocio central. 

*Bots.Domain.Entities*
Nessa camada estão as entidades do domínio. Essas por sua vez não possuem comportamento, ou seja, 
a construção foi feita com um viés "anêmico".

Estão os repositórios e todo o acesso a dados
*Bots.Repository*
Nessa camada estão os repositórios de acesso a dados.

*Bots.MicroService*
Camada responsável por expor os serviços que serão requisitados pela API.
Pra efeito do teste essa camada está na mesma solution. Porem num cenário distribuido pode-se pensar em os serviços
presentes nessa camada estarem, por exemplo, num arquitetura de microserviços;

*Api*
Camada responsável por entregar os dados solicitados para a aplicação
requisitante.
Inclui o swagger a fim de expor as rotas e documentação. Ao startar a aplicação coloque /swagger na url.

